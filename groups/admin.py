from django.contrib import admin

# Register your models here.
from groups import models


class GroupMemberInline(admin.TabularInline):
    model = models.GroupMember

# to change order of detail view in admin


class GroupAdmin(admin.ModelAdmin):
    # This order is show in group model in admin
    fields = ['name', 'description', 'slug']

    # multiple search
    search_fields = ['name', 'slug']

    # this is custom filter by name
    list_filter = ['name']

    # to view diffrent information of each object
    list_display = ['name', 'slug']

    # to edit somthing from list view in admin
    list_editable = ['slug']


admin.site.register(models.Group, GroupAdmin)
